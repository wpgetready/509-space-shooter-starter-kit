using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection.Emit;
//using System.Diagnostics;
//using NUnit.Framework.Internal.Filters;
//using Microsoft.Win32;
/*

Level 1 Easy
Level 2 Easy
Level 3 Medium
Level 4 Medium
Level 5 Boss 1
Level 6 Hard
Level 7 Hard
Level 8 Hard
Level 9 Hard
Level 10 Boss 2
Level 11 End Level
*/

public class SaveStats:MonoBehaviour{
	//Script saves score to game over scene
	public int _score;
	public string _previousLevel;
	public bool _levelComplete;
	public float _saveSoundVol;
	public float _saveMusicVol;

	public string _nextLeveL;
	public int _currentLives;
	public int _currentBullet;

	private string levelPrefsName="levelsEnabled";

	public string[] levelsNames;
	
	public static SaveStats instance;			//SaveStats is a singleton.	 SaveStats.instance.DoSomeThing();
	
	public void OnApplicationQuit() {				//Ensure that the instance is destroyed when the game is stopped in the editor.
	    instance = null;

	}
	
	public void Start() {
		if (instance != null){
	        Destroy (gameObject);			//Destroy if there is a SaveStats loaded
	    }else{
	        instance = this;				
	        DontDestroyOnLoad (gameObject); //Keep from deleting this gameObject when loading a new scene
	    }
	}


	///The following code uses some tricks to find out if a level is enabled, and also this data is stored in one integer only
	/// Every scene correspond a bit , so we have some limitations: game can't have more than 32 64 bits (however that is not the intention, maybe 20 is the upper top)
	/// In any case, this program can be modified using an string instead integer , having hundred of potential screens.
	/// HOWEVER, it WON'T WORK in Android, only in Unity Editor Mode , which is outrageuous for say the least.
	/// <summary>
	/// Find a scene in the AssetDatabase. Returns -1 if not found.
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="scene">Scene.</param>
	/// 
	/// 
	/// ABORTADO. Innecesariamente y estupidamente complejo para algo tan simple y estupido.
	/// SIMPLIFICADO!
	/// 
	/// 
	/*
	public int sceneIndexOld(string scene) {
		string[] path = { "Assets/Space Shooter/Scenes/Levels" }; //Buscar en esta carpeta
		string[] guids = AssetDatabase.FindAssets("Level", path); //Buscar el término Level en los objetos de la carpeta.
		for (int i = 0; i < guids.Length; i++) {
			if (AssetDatabase.GUIDToAssetPath(guids[i]).Contains(scene)) {
				return i;
			}
		}
		return -1;
	}
	*/

	/// <summary>
	/// Sorprendentemente AssetDatabase NO puede ser usado en aplicaciones Android , solo en modo Edición... para que sirve entonces?
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="scene">Scene.</param>
	public int sceneIndex(string scene) {
		for (int i = 0; i < levelsNames.Length; i++) {
			if (levelsNames[i].Contains(scene)) {
				return i;
			}
		}
		return -1;
	}

	public void enableLevel(string scene) {
		int sIndex = sceneIndex (scene);
		if (sIndex ==-1) {
			Debug.LogError (string.Format ("Error: Scene {0} not found!", scene));
		}
		char[] arrayLevels = getLevelsEnabled().ToCharArray ();
		arrayLevels [sIndex] = '1';
		string result = new string (arrayLevels);
		PlayerPrefs.SetString (levelPrefsName, result);
	}

	public bool isLevelEnabled(string scene) {
		int sIndex = sceneIndex (scene);
		if (sIndex ==-1) {
			Debug.LogError (string.Format ("Error: Scene {0} not found!", scene));
		}
		string currentLevel = getLevelsEnabled ();
		bool flag = (currentLevel.Substring (sIndex, 1) == "1");
		return flag;
	}

	public void createOrResetLevel() {
		PlayerPrefs.SetString (levelPrefsName,"10000000000000000000"); //20 potential levels, first level always enabled.
	}

	private string getLevelsEnabled() {
		if (!PlayerPrefs.HasKey (levelPrefsName)) {
			this.createOrResetLevel ();
		}
		string lvl  = PlayerPrefs.GetString (levelPrefsName);
		return lvl;
	}	
}