using UnityEngine;
using System;


public class Asteroid:MonoBehaviour{
	public float _hitpoints = 3.0f;			//How many hits a asteroid can take from a default bullet
	public int _points = 100;				//How many point player gets from destroying this asteroid
	public GameObject _breakInto;			//Contains the prefab that this asteroid spawnes when it is destroyed (can be blank)
	public float _debrisMultiplier = 1.0f;	//Change how much debris should be emitted from the _debrisPS particle system
	public float _dropChance;				//Chance asteroid will drop an item (multiplied by the GameController._dropMultiplier)
	public float _maxAsteroidSize = 1.25f;	//Asteroids are scaled at start for variety
	public float _minAsteroidSize = .75f;
	public static ParticleSystem _debrisPS;//The particle system that emits debris (this must be present on the stage as "Debris PS"
	
	public void Start() {
		if(GameController.instance._countFragments||!GameController.instance._countFragments && transform.parent==null)//Only count if this is not a fragment
		GameController.instance._asteroidCounter++;				//Increase counter that checks how many asteroids are on the stage
		if(_debrisPS == null){
		//Debug.Log("Found Debris > Should happen once");
		_debrisPS = GameObject.Find("Debris PS").GetComponent<ParticleSystem>();
		}
	}
	
	public void FixedUpdate() {
		//Max / Min Astreoid velocity (controlled by GameController)
		//Fixes issues where Asteroids builds up too much speed or stops completely, also good for altering difficulty
	    if(GetComponent<Rigidbody>().velocity.sqrMagnitude > GameController.instance._asteroidMaxVelocity){ 
	        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * GameController.instance._asteroidMaxVelocity;
	    } else if(GetComponent<Rigidbody>().velocity.sqrMagnitude < GameController.instance._asteroidMinVelocity){
	        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * GameController.instance._asteroidMinVelocity;
	    }
	    if(transform.position.y > -0.5f)
	    GameController.instance.CheckBounds(this.gameObject, null); 	// Check if the asteroid is in the game area, moves it to the closest edge if it is outside
	}
	
	public void Update() {
		if(_hitpoints < 0){											//When asteroid no longer has any hitpoints left
			int emit = 0;											
			if(this._breakInto != null){			
				GameController.instance.SpawnAsteroid(_breakInto, gameObject);
				emit = (int)(10*_debrisMultiplier);						//How many debris pieces to emit when asteroid breaks into pieces 
			}else{
				emit = (int)(25*_debrisMultiplier);						//How many debris pieces to emit when asteroid is destroyed	
			}
			_debrisPS.transform.position = transform.position;                              //Set Particle System position on asteroid position
	//FZSM: corregir
			ParticleSystem.MainModule main = _debrisPS.main;
			main.startColor = GetComponent<Renderer>().sharedMaterial.color;			//Change color of debris to match asteroid
			_debrisPS.Emit(emit);															//Start emission	 
			if(GameController.instance._countFragments||!GameController.instance._countFragments && transform.parent==null)//Only count if this is not a fragment
			GameController.instance._asteroidCounter--;										//Decrease counter that checks how many asteroids are on the stage
			if(_dropChance * GameController.instance._dropMultiplier > UnityEngine.Random.value){
				GameController.instance.Drop(transform.position);
			}
			GameController.instance.AddPoints(_points);										//Adds points to score
			GameController.instance.InvokeAsteroids();										//Start routine to check if game should spawn more asteroids
			Destroy(gameObject);
		}else{	
			Vector3 newPos = transform.position;
			if(transform.position.y < -0.01f){												//Checks to see if asteroid is on the game plane
				newPos.y = Mathf.Lerp(transform.position.y, 0.0f, Time.deltaTime*GameController.instance._asteroidAlignToPlaneSpeed); //Gradually positiones the asteroid closer to the game plane
			}else{		
				newPos.y = 0.0f;															// Keeps the asteroid on the game plane
			}
			transform.position = newPos;
		}
	}
//NOT USED
//function RandomForce (amount:float) {
//	
//	rigidbody.AddTorque(Vector3(Random.Range(100,-100),Random.Range(100,-100),Random.Range(100,-100))*amount);
//	rigidbody.AddForce(Vector3(Random.Range(1000,-1000),Random.Range(1000,-1000),Random.Range(1000,-1000))*amount);
//
//}

}
