using UnityEngine;
using System;


public class Stars:MonoBehaviour
{
	public float _speed = 1.0f;
	
	public void Update() {
		transform.position = Vector3.Lerp(transform.position, Player.instance.transform.position*-1, Time.deltaTime*_speed);	//Move the stars negatively based on player position to simulate paralax scroll
	}
}
