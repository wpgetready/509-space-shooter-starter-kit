﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TextFader : MonoBehaviour {

	public GameObject Text3D;
	public float timeBeforeStart = 1.0f;
	// Use this for initialization
	IEnumerator Start () {
		Text3D.SetActive (true);
		yield return (new WaitForSeconds (timeBeforeStart));
		Text3D.transform.DOMove ( gameObject.transform.position, 10.0f, false).OnComplete(Deactivate);
	}

	private void Deactivate() {
		Text3D.SetActive (false);
	}
}
