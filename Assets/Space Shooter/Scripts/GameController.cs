using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController:MonoBehaviour{
	public AudioClip[] _startMusic;					//Music to play when the game starts
	//Difficulty modifiers are best used in survival mode
	public int _difficultyIncreaseAsteroidCount;		//Ever x amount of asteroids spawned will increase difficulty
	public float _maxDifficulty = 3.0f;					//Difficulty will stop increasing after reaching this value
	public float _difficultyMultiplierIncrease = .1f;	//Increase this amount everytime difficulty is increased
	public float _difficultyMultiplier = 1.0f;			//INITIAL difficulty value. It increases asteroid hitpoints and max asteroids.
														//Hitpoint = how many strikes to make an asteroid to be fragmented/destroyed
														//Every time the difficulty is increased, _difficultyMultiplierIncrease is added to _difficultyMultiplier
	//Summing up: _difficultyIncreaseAsteroidCount = used on survival games, to increase level steady. Use it in mid to hard games. 0 = disable difficulty
	//							    _maxDifficulty = top difficulty before game is too hard. Put a higher value (10 for example) for very hard games.
	//				 _difficultyMultiplierIncrease = Delta increase. When higher delta, difficult increase faster
	//						 _difficultyMultiplier = Initial difficulty. For hard games, better start low, and use a higher delta increase. 
	//												 Use 1 as standard start
	//Examples: survival (5,3,.25,0) = Difficulty starts in zero (VERY easy) and increases 0.25 every 5 asteroids to a limit of 3
	//          Easy     (0,0,0,1)  =  Difficulty normal (1) and fixed, without changes.
	//          Medium   (5,2,.25,1) = Difficulty normal (1) increasing .25 to 2 every 5 asteroirds. It is inferred that the game needs AT LEAST ((2-1) /.25 +1 ) *5 = 25 asteroids to reach full difficulty.
	//			Hard     (3,3,.5,1)  = Difficulty starts normal but it is increased rapidly. The level would need ((3-1) /.5 +1) *3 = 15 asteroids to reach full difficulty.
	//		(very?)	Hard     (2,3.5,.5,1)  =  The level will reach full difficulty on ((3.5-1) /.5 +1 ) *2 = 12 asteroids.

	public float _asteroidMinVelocity = 1.0f;				//Keeps Asterois from stopping to prevent dull gameplay. Use 1 as standard, except some special difficult games.
	public float _asteroidMaxVelocity = 2.0f;				//Keeps Asterois from gaining too much velocity . Example: 3 is a fast asteroid.

	public Asteroid[] _asteroids;						//List of asteroids to spawn at random
	public Material[] _asteroidMaterials;				//Materials will be changed randomly on asteroids if this list contains any (adds some visual varaity to the asteroids)

	public float _asteroidSpawnDelay = 0.5f;			//Delay between each new asteroid. Example: Easy: >1 Medimum & Hard = 0.5
	public int _maxAsteroids = 3;						//Max asteroids and fragment on the screen (will not spawn new asteroids when there are this many in the game). Example: Easy = 3 / Medium = 5,7  / Hard =10 +
	public int _spawnAsteroidAmount = 5;				//How many asteroids to spawn for this game . -1 will spawn asteroids infinetly (survival mode)
	public float _asteroidAlignToPlaneSpeed = 1.0f;		//How fast asteroids moves to the game area (bigger number makes it harder to avoid new asteroids, can be used for increasing difficulty) Example: Hard = 2
	public bool _spawnAsteroidsWhenEnemy;			   //Turn this on if asteroids should spawn even if a enemy is on stage. Turn this on for hard games.
	public float _spawnAsteroidsWhenEnemyDifficultyOveride = 2.5f; //At this difficulty asteroids will spawn even if there is enemies in the game 
																	//So, difficulty needs to reach this level first! Plan carefully the values...

	public bool _countFragments=true;				//Counts fragments as asteroids (disable this to ignore asteroid fragments when spawning new asteroids). Disable it for hard games.
	
	public Enemy[] _enemy;								//List of enemies like UFO that spawns less frequently than asteroids
	public int _maxEnemy = 1;							//Max enemies in the game at once
	public int _spawnEnemyAmount = 5;					//-1 will spawn enemies infinetly (survival mode)
	public float _spawnEnemyEverySecond = 3.0f;
	public float _spawnEnemyChance = .1f;				//Chance that an enemy will be spawned 1=always 0=never. This probability it is used every time we try to spawn the enemy.

	//Examples:
	// (1,-1,3,.1) = survival (easy). Spawn one enemy at time, to infinitely.
	// (1,5,3,.1) = spawn 1 enemy at time to a max total of five. Try every 3 seconds with a chance of 10% of appearing. (easy and medium games)
	// (2,6,3,.2) = spawn to 2 enemies at same time in screen to a max of 5. Try every 3 seconds with chance of 20% of appearing (medium)
	// (2,6,3,.25) = medium/hard
	// (3,6,2,.1) = hard
	// (3,9,3,.25) = hard
	// (1,1,1,.2) = special case: boss. We spawn one time only, and we try every one second until it appears. This is usually combined with an special asteroid configuration, where asteroids appears
	//here and there



	//Powerups, lives and score multipliers
	public Pickup _pickup;								//Pickup objects that drops
	public float _dropMultiplier = 1.0f;					//chance that items drop
	public float _bombDropRate	= 0.1f;					//chance that bomb item drop
	public float _lifeDropRate = 0.05f;					//chance that life item drop
	
	public int _score;
	
	public TextMesh _highScoreTextMesh;				//GUI score text
	public TextMesh _scoreTextMesh;					//GUI score text
	public TextMesh _scoreMultiplierTextMesh;			//GUI score text
	public int _maxScoreMultiplier=100;
	
	public GameObject[] _lives;						//List of life gameobjects containing model (Future update note: make dynamic)
	//Anchors (GUI)
	//Attach gameObject containing 3D GUI elements that is to be aligned to the screen
	public GameObject _guiTopLeft;			
	public GameObject _guiBottomLeft;
	public GameObject _guiTopRight;
	public GameObject _guiBottomRight;
	
	public string _gameOverScene = "Game Over SCN";	//Scene to load when player dies --or game is complete(OBSOLETE)
	public string _gameNextLevelScene="Next Level SCN";//Scene to Load when the player complete the game
	public string _gameNextLevel= ""; //Next Level to load to pass to Game Next Level SCN

	public bool _bossScene = false; //indica si esta pantalla es de bosses. Cuando esto es asi, al matar el / los bosses se termina el juego, sin chequear el tema de los asteroides.
	public int  _maxBosses = 1; //How many bosses are in this screen.
	public int _bossCount =0; //Boss counter. when _bossCount =maxBosses screen is finished.

	[HideInInspector] 
	public int _scoreMultiplier = 1;			//Score is multiplied by this value
	[HideInInspector] 
	public int _toPoints;
	[HideInInspector] 
	public int _asteroidCounter;
	[HideInInspector] 
	public int _enemyCounter;
	
	float _gameWidth;					//Width of the game area
	float _gameHeight;					//Height of the game area
	//Calculated half of the screen; limits the calculation to once the game starts
	float _gameWidthHalf;	
	float _gameHeightHalf;
	//These values contain information about the edges of the screen, calculated once the game start
	RaycastHit _bottomLeft;
	RaycastHit _bottomRight;
	RaycastHit _topRight;
	RaycastHit _topLeft;
	int _spawnEnemyCounter;		
	int _totalAsteroidsCounter;
	int _totalEnemyCounter;
	int _currentTrack;

	//private AdsManager AdsM;
	
	public static GameController instance;			// GameController is a singleton.	 GameController.instance.DoSomeThing();
	
	
	public void OnApplicationQuit() {					// Ensure that the instance is destroyed when the game is stopped in the editor.
	    instance = null;
	}

	void Awake() {
		//AdsM = new AdsManager (); //Initializes ads
		if (SaveStats.instance._levelComplete) {
			_toPoints = SaveStats.instance._score;
			CountPoints ();
		}
	}

	public IEnumerator Start() {
		instance =  (GameController)FindObjectOfType(typeof(GameController));
		AdsManager.instance.RequestInterstitial (); //I need an ad before everything.

		CalcBounds();									//Calculates the game area based on camera edges by casting rays from all corners of the main camera
		_highScoreTextMesh.text = "" +PlayerPrefs.GetInt(SceneManager.GetActiveScene().name+"highscore").ToString("000000000");
		//if(_difficultyMultiplier<1)_difficultyMultiplier=1;	//Make sure difficulty is not lower than 1
		//_scoreTextMesh.text = _score.ToString("000000000");	//Resets the score text
		GUIPosition ();										//Positiones the 3D GUI
		InvokeRepeating("CountPoints", 1.0f, .01f);				//Repeating function that counts the score
		InvokeAsteroids();									//Spawns asteroids until max spawn amount has been reached
		InvokeEnemies ();									//Spawns enemies until max spawn amount has been reached
		yield return new WaitForSeconds(.01f);							//Wait a little while before accessing SoundController to make sure it is there
		if(_startMusic.Length>0)
			SoundController.instance.PlayMusic(_startMusic[0], 1.5f, 1.0f, true);	//Play music
	}
	
	public void IncreaseDifficulty() {
		if(_totalAsteroidsCounter >0 && _difficultyIncreaseAsteroidCount > 0 && _difficultyMultiplier<this._maxDifficulty && _totalAsteroidsCounter%_difficultyIncreaseAsteroidCount== 0){
			//Debug.Log("Increased Difficulty : " + _totalAsteroidsCounter);
			if(_difficultyMultiplier >= _spawnAsteroidsWhenEnemyDifficultyOveride)	//At this difficulty asteroids will spawn even if there is enemies in the game 
			this._spawnAsteroidsWhenEnemy = true;								
			_difficultyMultiplier+=_difficultyMultiplierIncrease;					//Increase the multiplier value
		}
	}
	
	//Called by asteroids to create new asteroids, prefabs instantiating prefab of same class can not be done
	public void SpawnAsteroid(GameObject go,GameObject spawner) {	
		GameObject asteroid = (GameObject)Instantiate(go, spawner.transform.position, spawner.transform.rotation);	
		Asteroid[] children = null;
		children = asteroid.GetComponentsInChildren<Asteroid>(true);			//Makes a list of all the asteroid children
		for(int i = 0; i < children.Length; i++) {
			children[i].GetComponent<Renderer>().sharedMaterial =spawner.GetComponent<Renderer>().sharedMaterial;
			children[i].GetComponent<Rigidbody>().velocity = spawner.GetComponent<Rigidbody>().velocity;		//Copy the velocity of main asteroid to children when it breaks	
			children[i].transform.localScale = spawner.transform.localScale;	//Resizes asteroids	//Might generate Android lag (Noticed some tiny lagspikes)
			children[i]._hitpoints *=_difficultyMultiplier;						//Increase hitpoints based on difficulty
		}
	}
	
	public void SpawnEnemy() {	
			if(_asteroidCounter > 2 && _spawnEnemyChance >= UnityEngine.Random.value && (_totalEnemyCounter < _spawnEnemyAmount || _spawnEnemyAmount < 0)){	//Check if all enemies in level has been spawned 
				//Debug.Log("running"  + _enemyCounter +" enemies "+_maxEnemy * Mathf.Floor(this._difficultyMultiplier));
				int d = 1;
				if(_difficultyMultiplier > 1)d= (int)Mathf.Floor(this._difficultyMultiplier);
				if(_enemyCounter < _maxEnemy * d)	{		
					Instantiate(_enemy[UnityEngine.Random.Range(0, _enemy.Length)].gameObject, new Vector3(UnityEngine.Random.Range(-_gameWidthHalf, _gameWidthHalf)*.5f, (float)UnityEngine.Random.Range(-10, -20), UnityEngine.Random.Range(-_gameHeightHalf, _gameHeightHalf)*.5f), Quaternion.identity);	
					_totalEnemyCounter++;							//Counts how many enemies have spawned
				}
			}
	}
	
	public void SpawnAsteroids() {									
			if((_spawnAsteroidAmount < 0 || _totalAsteroidsCounter < _spawnAsteroidAmount) && (_enemyCounter <=0 || _spawnAsteroidsWhenEnemy)){	//Check if all asteroids in level has been spawned 
				if(_asteroidCounter < _maxAsteroids + _difficultyMultiplier-1)	{
					GameObject asteroid = (GameObject)Instantiate(_asteroids[UnityEngine.Random.Range(0, _asteroids.Length)].gameObject, new Vector3(UnityEngine.Random.Range(-_gameWidthHalf, _gameWidthHalf), (float)UnityEngine.Random.Range(-10, -20), UnityEngine.Random.Range(-_gameHeightHalf, _gameHeightHalf)), Quaternion.identity);
					Asteroid a = asteroid.GetComponent<Asteroid>();
					asteroid.transform.localScale *= UnityEngine.Random.Range(a._maxAsteroidSize, a._minAsteroidSize);	//Resizes asteroids based on min/max variables	///Might generate Android lag (Noticed some tiny lagspikes)
					asteroid.GetComponent<Rigidbody>().AddForce((float)UnityEngine.Random.Range(-5,5), 0.0f , (float)UnityEngine.Random.Range(-5,5));
					a._hitpoints *=_difficultyMultiplier;			//Increase hitpoints based on difficulty
					if(_asteroidMaterials.Length > 0){				//Changes material of asteroids if there are any in the array
						asteroid.gameObject.GetComponent<Renderer>().sharedMaterial = _asteroidMaterials[UnityEngine.Random.Range(0,_asteroidMaterials.Length)];
						//Changes material of the asteroids children to match the new material
						Renderer[] children = null;
						children = asteroid.GetComponentsInChildren<Renderer>(true);	//Makes a list of all the renderers in the asteroid children
						for(int i = 0; i < children.Length; i++) {
							children[i].GetComponent<Renderer>().sharedMaterial =asteroid.gameObject.GetComponent<Renderer>().sharedMaterial;
						}
					}
					_totalAsteroidsCounter++;						//Counts how many asteroids have spawned in total (Fragments not included)
					IncreaseDifficulty();							//Increases difficulty based on how many asteroids have spawned
				}else{
					CancelInvoke("SpawnAsteroids");					//Stops spawning asteroids when max limit has been reached
				}
		}else if (_enemyCounter<=0 && _asteroidCounter <=0 && !_bossScene) { //Finish if this is a no boss scene..
								StartCoroutine(GameOver(true));
		}
	}
	
	public void InvokeEnemies() {	//Spawns enemies until max spawn amount has been reached
		CancelInvoke("SpawnEnemy");
		if(_spawnEnemyChance > 0)
			InvokeRepeating("SpawnEnemy", _spawnEnemyEverySecond, _spawnEnemyEverySecond);	//Start spawning enemies
	}
	
	public void InvokeAsteroids() {	//Spawns asteroids until max spawn amount has been reached
		CancelInvoke("SpawnAsteroids");
		InvokeRepeating("SpawnAsteroids", _asteroidSpawnDelay, _asteroidSpawnDelay);
	}
	
	
	public void AddPoints(int points) {						//Adds points to score
		_toPoints+=points*_scoreMultiplier;
	}
	
	public void SetScoreMultiplier(int amount) {
		_scoreMultiplier = amount;
		_scoreMultiplierTextMesh.text = "x" + _scoreMultiplier;
	}
	
	public void AddScoreMultiplier(int amount) {
		if(_scoreMultiplier < _maxScoreMultiplier){
			_scoreMultiplier += amount;
				_scoreMultiplierTextMesh.text = "x" + _scoreMultiplier;
		}
	}

	/// <summary>
	/// Counts the points. I also added check for bosses scene because it is faster and Count Points is called every one second.
	/// </summary>
	public void CountPoints() {								//Adds points to score
		if(_toPoints > _score){
			_score+=  (int)Mathf.Ceil((_toPoints-_score)*.1f);
			_scoreTextMesh.text = _score.ToString("000000000");
		}
		if (_bossScene){ 
			if (_maxBosses == _bossCount) { //If we kill all bosses, finish this.
				StartCoroutine(GameOver(true));
			}
		}
	}
	
	public void Drop(Vector3 pos) {							//Drop an item on the play field
			if(Player.instance._inControl)					//Won't drop anything if player has just died
			Instantiate(_pickup, pos , _pickup.transform.rotation);
	}
	
	// Calculates the game area based on camera edges by casting rays from all corners of the main camera
	public void CalcBounds() {
				
				if(Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.0f, 0.0f, 0.0f)),out _bottomLeft)){
					Vector3 bottomLeft = _bottomLeft.point;
					bottomLeft.y = 0.0f;
	    			_bottomLeft.point = bottomLeft;   		
	    		}		
	    		if(Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(1.0f, 0.0f, 0.0f)),out _bottomRight)){
					Vector3 bottomRight = _bottomRight.point;
					bottomRight.y = 0.0f;
	    			_bottomRight.point = bottomRight;		
	    		}
	    		if(Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(1.0f, 1.0f, 0.0f)),out _topRight)){
	    			Vector3 topRight = _topRight.point;
					topRight.y = 0.0f;
	    			_topRight.point = topRight;
	    		}
	    		if(Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.0f, 1.0f, 0.0f)),out _topLeft)){
	    			Vector3 topLeft = _topLeft.point;
					topLeft.y = 0.0f;
	    			_topLeft.point = topLeft;
	    		}
	    		//Set game are area
				_gameWidth = Vector3.Distance(_bottomLeft.point, _bottomRight.point);
				_gameHeight = Vector3.Distance(_bottomLeft.point, _topLeft.point);
	 			//Calculate half sizes once to avoid doing this multiple times in other functions
		_gameWidthHalf = _gameWidth * .5f  +.75f;
		_gameHeightHalf = _gameHeight * .5f+.75f;
	}
	
	//Positiones the 3D GUI
	public void GUIPosition() {
		_guiTopLeft.transform.position = _topLeft.point;
		_guiBottomRight.transform.position = _bottomRight.point;
		_guiTopRight.transform.position = _topRight.point;
		_guiBottomLeft.transform.position = _bottomLeft.point;
	}
	
	//Visuals to be able to see the play area and other information in the editor
	public void OnDrawGizmos() {
		if(!Application.isPlaying)									//Only calculate repeatedly when not in play mode
			CalcBounds();
			Gizmos.DrawCube(_bottomLeft.point, Vector3.one*.1f);			//Draw cubes on game area corners (without padding)
			Gizmos.DrawCube(_bottomRight.point, Vector3.one*.1f);	
			Gizmos.DrawCube(_topRight.point, Vector3.one*.1f);
			Gizmos.DrawCube(_topLeft.point, Vector3.one*.1f); 		
		 	Gizmos.DrawWireCube (transform.position, new Vector3 (_gameWidth,0.0f,_gameHeight));	//Draw wires for padded game area
	}
	
	//Check if a gameObject is outside the game area (this is run by objects that wraps edges when leaving area)
	//CheckBounds (	obj: 	object to wrap, 
	//				ps: 	particle system to disable then re enable, (world particle systems sometimes leaves a trail when moved instantly)(set to null if no particle system)
	public void CheckBounds(GameObject obj,ParticleSystem ps) { 	
			if(obj.transform.position.x > _gameWidthHalf){
				obj.transform.position = new Vector3(-_gameWidthHalf, obj.transform.position.y,obj.transform.position.z);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}else if(obj.transform.position.x < -_gameWidthHalf){
				obj.transform.position = new Vector3(_gameWidthHalf, obj.transform.position.y,obj.transform.position.z);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}
			if(obj.transform.position.z > _gameHeightHalf){	
				obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, -_gameHeightHalf);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}else if(obj.transform.position.z < -_gameHeightHalf){
				obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, _gameHeightHalf);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}
	}
	//padding: some objects like pickups need to be further inside the screen to be visible and touchable/clickable, added some padding to fix
	public void CheckBoundsInverted(GameObject obj,ParticleSystem ps,float padding) { 	
			if(obj.transform.position.x > _gameWidthHalf+-padding){	
				obj.transform.position = new Vector3(_gameWidthHalf -padding, obj.transform.position.y,obj.transform.position.z);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}else if(obj.transform.position.x < -_gameWidthHalf+padding){
				obj.transform.position = new Vector3(-_gameWidthHalf +padding, obj.transform.position.y,obj.transform.position.z);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}
			if(obj.transform.position.z > _gameHeightHalf-padding){	
				obj.transform.position =  new Vector3(obj.transform.position.x, obj.transform.position.y, _gameHeightHalf -padding);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}else if(obj.transform.position.z < -_gameHeightHalf+padding){
				obj.transform.position = new Vector3(obj.transform.position.x, obj.transform.position.y, -_gameHeightHalf +padding);
				if(ps != null)StartCoroutine(DisableEnablePS(ps));
			}
	}
	//disables then adds delay to re enable, fixes particle trails
	public IEnumerator DisableEnablePS(ParticleSystem ps){
		ParticleSystem.EmissionModule emission = ps.emission;
		if (emission.enabled){
			emission.enabled = false;
	 		yield return new WaitForSeconds(.05f);
	 		if((ps != null) && ps.transform.parent.GetComponent<Collider>().enabled)
			emission.enabled = true;
	 	}
	}
	
	//destroy all gameobjects with tag (Use with causion, Find is heavy on mobile)
	public void DestroyAll(string tag) {
		GameObject[] gameObjects =  GameObject.FindGameObjectsWithTag (tag);
	    for(int i = 0 ; i < gameObjects.Length ; i ++)
	        Destroy(gameObjects[i]);
	}
	
	//Create a force explosion
	public void Explosion(Vector3 explosionPos,float radius,float power) {
	        Collider[] colliders = Physics.OverlapSphere (explosionPos, radius);     
	        foreach(Collider hit in colliders) {
	            if (hit == null)
	                continue;           
	            if ((hit.GetComponent<Rigidbody>() != null) && hit.gameObject.tag!="Bullet")
	                hit.GetComponent<Rigidbody>().AddExplosionForce(power , explosionPos, radius);
	        }
	}
	
	//Checks player status to correspond with GUI lives
	public void CheckPlayer() {
		for(int i = 0; i < _lives.Length; i++){	
			if(i>=Player.instance._lives){		
				_lives[i].SetActive(false);	
			}else if(i<=Player.instance._lives){
				_lives[i].SetActive(true);
			}	
		}
	}
	
	//Function is run when player dies or game is complete
	public IEnumerator GameOver(bool levelComplete) {
		

		SoundController.instance.PlayMusic(null, 1.5f, 1.0f, true);				//Fade down the music
		yield return new WaitForSeconds(2.0f);											//Wait a little while before changing level
		SoundController.instance.PlayMusic(_startMusic[1], 1.5f, 1.0f, true);	//Start playing game over music
		SaveStats.instance._score=this._score;								//Saves score

		SaveStats.instance._previousLevel = SceneManager.GetActiveScene().name;	//Saves this levels name so that it can be used in a replay button
		SaveStats.instance._levelComplete = levelComplete;                  //Did the player die or complete the level

		#if UNITY_EDITOR
			Debug.Log("Editor Mode, No Ads");
		#else
			AdsManager.instance.DisplayIAd (); //display ad if possible.
		#endif

		if (levelComplete) {
			SaveStats.instance._nextLeveL = _gameNextLevel; 	//Save the next scene to load
			SaveStats.instance._currentLives = Player.instance._lives; //Save current lives, recharge when starts, see Player.cs-> Awakes
			SaveStats.instance._currentBullet = Player.instance._currentBullet; //WARNING: That's implying ALL players in every level have the same bullets array in the same order!!!!!
			SaveStats.instance.enableLevel (_gameNextLevel);
			SceneManager.LoadScene(_gameNextLevelScene);		//Go to the next level.
		} else {
			SceneManager.LoadScene(_gameOverScene);								//Load game over scene
		}

	}
	//If game has many sounds this can used to change music trough the pause menu
	public void NextTrack() {													
		_currentTrack++;													//Next music track
		if(_currentTrack >= _startMusic.Length){							//Check music list lenght in case reached the end
			_currentTrack = 0;
		}
		if(Time.timeScale > 0)			//Check if in pause mode
		SoundController.instance.PlayMusic(_startMusic[_currentTrack], 1.5f, 1.0f, true);	//Fade = true
		else
		SoundController.instance.PlayMusic(_startMusic[_currentTrack], 1.5f, 1.0f, false);	//Fade = false (can't fade when timeScale = 0)
	}
}
