using UnityEngine;
using System;
using System.Collections;


public class Pickup:MonoBehaviour{
	//Bomb pickup properties
	public GameObject _bomb;			//Model of Bomb
	public AudioClip _bombSound;		//Sound when Bomb picked up
	//Life pickup properties
	public GameObject _life;			//Model of Life
	public AudioClip _lifeSound;		//Sound when Life picked up
	//Point pickup properties
	public GameObject _point;			//Model of Point
	public AudioClip _pointSound;		//Sound when Point picked up
	public float _lifetime = 10.0f;		//How long is the pickup item alowed to be on the stage
	public string _type;				//Name of the Pickup object (bomb, life, point etc)
	
	bool _pickedUp;	//Has this object been picked up (start the movement towards player)
	Vector3 _moveFrom;	//Postion of the Pickup once it gets picked up
	int _moveCounter;	//Counter for movement over time(Lerp)
	GameObject _model;	//Gameobject based on what type of item this is
	
	
	public void Start() {
		if(_type == ""){
			float r = UnityEngine.Random.value;
			if(r < GameController.instance._lifeDropRate && Player.instance._lives < Player.instance._maxLives)	_type = "life";								//Check if life should drop
			else if(r < GameController.instance._bombDropRate && Player.instance._currentBullet <  Player.instance._bullets.Length-1)	_type = "bomb";		//If no life is dropped, check for bomb
			else _type = "point";							//If no other drops, drop point
		}	
		//Activate model based on _type
		if(_type == "bomb")	{
			_bomb.GetComponent<Renderer>().enabled = true;
			_model = _bomb;
		}else if(_type == "point"){
			_point.GetComponent<Renderer>().enabled = true;
			_model = _point;
		}else if(_type == "life"){
			_life.GetComponent<Renderer>().enabled = true;
			_model = _life;
		}
		StartCoroutine(AutoDestroy());													//Start the countdown to remove object automaticly
		GameController.instance.CheckBoundsInverted(gameObject, null, 1.0f);	//Checks if gameObject is outside the game area
	}
	
	//Call to activate the movement towards player
	public void Pick() {	
		_pickedUp = true;
		_moveFrom = this.transform.position;
	}
	
	//Blinking before destroyed
	public IEnumerator AutoDestroy() {	
		yield return new WaitForSeconds(_lifetime -3);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.5f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.2f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.4f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.3f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.3f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.2f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.2f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = false;
		yield return new WaitForSeconds(.1f);
		_model.GetComponent<Renderer>().enabled = true;
		yield return new WaitForSeconds(.1f);
		Destroy(this.gameObject);
	}
	
	public void FixedUpdate() {
		if(!Player.instance._inControl){		//Destroy pickup if player is dead
			Destroy(this.gameObject);
		}else if(!_pickedUp){
			//transform.Rotate(Vector3(0,0,100*Time.deltaTime));										//Rotates the gameobject (effect)		
			if(Vector3.Distance(Player.instance.transform.position, this.transform.position) < 2 ){	//Pickup if close to player
				this.Pick();	
			}	
		}else{
			_moveCounter++;																										//Increase counter for movement over time(Lerp)
			_model.GetComponent<Renderer>().enabled = true;			
			StopCoroutine("AutoDestroy");																		
			transform.position = Vector3.Lerp(_moveFrom, Player.instance.transform.position, Time.deltaTime*_moveCounter*2);	//Move towards player
			if(Vector3.Distance(Player.instance.transform.position, this.transform.position) < .25f){							//Check distance to player, if close enough it will trigger event based on _type	
				if(_type == "point"){																							//Adds to score multiplier
					if(_pointSound != null) SoundController.instance.Play(_pointSound, 2.0f, UnityEngine.Random.Range(1.5f,1.25f));						//Play sound
					GameController.instance.AddScoreMultiplier(1);
				}else if(_type == "bomb"){																						//Upgrade weapon
					if(_bombSound != null) SoundController.instance.Play(_bombSound, 2.0f, 1.25f);											//Play sound
					Player.instance.UpgradeWeapon();											
				}else if(_type == "life"){																						//Add life to player stats
					if(_lifeSound != null) SoundController.instance.Play(_lifeSound, 2.0f, 1.25f);											//Play sound
					if(Player.instance._lives < Player.instance._maxLives)
					Player.instance._lives++;				
				}
				GameController.instance.CheckPlayer();		//Update player stats (GUI)
				Destroy(this.gameObject);					//Remove gameObject
			}
		}
	}
}
