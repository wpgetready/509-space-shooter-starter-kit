using UnityEngine;
using System;
using System.Collections;
using CnControls;

public class Player:MonoBehaviour{
	public Bullet _bullet;							//Contains bullet to be fired when shooting
	public bool _clickToPickup;					//Enable if user can touch/click pickup objects to pick them up
	public GameObject _shipModel;					//Model of the spaceship
	public GameObject _shield;						//Shield model
	public ParticleSystem rocket;					//Rocket particle system
	public ParticleSystem _explosion;				//Explosion particle system when player dies
	public GameObject _moveTo;						//Player always follows this around
	public GameObject _moveLook;					//Rotation of this controls direction of players movement, added to make the ship be able to rotate towards the mouse/touch but move forward based on this object
	public int _lives = 3;							//Players lifes
	public int _maxLives = 3;						//Players maximum allowed lives (Remember to change the GUI prefab lives if this is increased)
	public GameObject _bulletPoint;				//GameObject that controls where the bullets spawn (look in player prefab)
	public Bullet[] _bullets;						//List of bullets
	public int _currentBullet;						//Index of current bullet in list
	public AudioClip _audioDeath;					//Audio clip when player dies
	public RaycastHit hit;						//Mouse/touch position
	public float _speed = 0.5f;						//Ship movement speed
	public float _rotateSpeed = 3.0f;					//Ship rotation speed
	bool _invunerable;			//Can the player be hurt
	public bool _inControl = true;		//User control of the ship
	bool _shooting;				//Ship shooting

	public float _invulnerableTimeStart = 5.0f;
	public float _invulnerableTimeRespawn = 3.0f;
	
	Vector3 pos;					//Raycast position
	public static Player instance;				// Player is a singleton. Player.instance.DoSomeThing();
	
	
	public void OnApplicationQuit() {				// Ensure that the instance is destroyed when the game is stopped in the editor.
	    instance = null;
	}

	void Awake() {
		if (SaveStats.instance._levelComplete) {
			_lives = SaveStats.instance._currentLives; //Restore current lives if the player goes from previous level
			_currentBullet = SaveStats.instance._currentBullet; //WARNING: this implies ALL levels have the SAME array with the same bullet order(!)
			this._bullet = this._bullets[_currentBullet];
		}
	}

	public IEnumerator Start() {
		instance =  (Player)FindObjectOfType(typeof(Player));
		//Setting parent to null is very tough on mobile, if player is instantiated this has to be changed
		_moveTo.transform.parent = null;		//Put follow object in the stage root to avoid it flailing around with the players movement
		_explosion.transform.parent = null;
		_bulletPoint.SetActive(false);			//Disables the bullet spawn point model
		//_shield.active = false;				//Disables the shield model
		StartCoroutine(Invunerable(_invulnerableTimeStart));							//Make player invunerable to damage for 5 seconds when game starts
		yield return new WaitForSeconds(.1f);
		GameController.instance.CheckPlayer();	//Update player stats (GUI)
	}
	
	//Adds bullets to the stage(note: add multiple weapon types? _type if(_type))
	public void Shoot() {
		if(_inControl){							//Cannot shoot while disabled
			Instantiate(_bullet, _bulletPoint.transform.position, transform.rotation);	//Create a bullet on the stage
			_shooting = true;					//Player is shooting
		}
	}
	
	public void StopShoot() {
		CancelInvoke();							//Stops shooting
	}
	
	public void OnCollisionEnter(Collision col) {
		//If player hits a asteroid and is not invunerable/shielded
		if(!_invunerable){
			if (col.gameObject.tag == "Asteroid"){
				col.gameObject.GetComponent<Asteroid>()._hitpoints = -1.0f;	//Destroy/break the asteroid
				StartCoroutine(DestroyShip());	//Disable ship for a while to reset position
			}else if (col.gameObject.tag == "Bullet"){
				StartCoroutine(DestroyShip());	//Disable ship for a while to reset position
			}
		}
	}
	
	//Disables the ship and player controll for a while and removes one life (note: add smooth reset position?)
	public IEnumerator DestroyShip() {
		_explosion.transform.position = transform.position;		//Positiones the explosion ontop of player
		_explosion.Play();										//Play explosion particle system
		GameController.instance.SetScoreMultiplier(1);			//Reset the score multiplier when dead
		_lives -=1;												//Remove a life
		if(_audioDeath != null){										//If there is audio on death
			SoundController.instance.StopAll();					//Stop all other sounds (to make this sound more audiable)
			SoundController.instance.Play(_audioDeath, 2.0f, 1.0f);	//Play the death sound
		}
		_invunerable = true;									//Make ship take no damage for a limited time while resetting
		GetComponent<Collider>().enabled = false;								//Disable ship collider
		_inControl = false;										//Player can no longer be controlled by user
		//_moveTo.SetActiveRecursively(false);					//Disable the moveto object (GUI)
		GameController.instance.CheckPlayer();                  //Update player stats (GUI)

		ParticleSystem.EmissionModule emission = rocket.emission;

		emission.enabled = false;							//Disable the rocket particle system

		//yield(WaitForSeconds(1));							//Delay 
		this._shipModel.GetComponent<Renderer>().enabled = false;
		transform.position = Vector3.zero;						//Set position to absolute zero
		//GameController.instance.DestroyAll("Pickup");
		_moveTo.transform.position = transform.position;		//Reset the moveTo gameObject
		//FZSM: A trick for reusing pickup example. Didnt' work
		//GameController.instance.CheckBoundsInverted (_moveTo, null, 50.0f);
		if(this._lives >= 0){									//Check if player has any lives left
			yield return new WaitForSeconds(5.0f);							//Delay
			this._shipModel.GetComponent<Renderer>().enabled = true;
			StartCoroutine(Invunerable(_invulnerableTimeRespawn));										//Ship will not take damage for 2 seconds
			_inControl = true;                                  //Player is now controllable again
			emission.enabled = true;						//Enable rockets
			ResetWeapon();										//Set to starter weapon(bullet)
			StopShoot();										//Stops shooting
		}else{
			StartCoroutine(GameController.instance.GameOver(false));			//GameOver if lives are less than zero (false indicates that the level was not completed)
		}
	}
	
	public IEnumerator Invunerable(float sec) {	
		_shield.SetActive(true);									//Show shield model
		yield return new WaitForSeconds(sec);								//Delay
			_shield.SetActive(false);								//Disable shield
			_invunerable = false;								//Player is no longer invunerable
			GetComponent<Collider>().enabled = true;							//Enable collider
	}
	
	//Make the ship invunerable for a duration (Note: in progress)
	public IEnumerator invunerable(){
		_invunerable = true;
		yield return new WaitForSeconds(1.0f);
		_invunerable = false;
	}
	
	//Upgrade weapon by picking the next Bullet in the _bullets list
	public void UpgradeWeapon() {
		if(_currentBullet < _bullets.Length-1){
			StopShoot();													//Stops shooting so that the Shoot function get updated
			_currentBullet++;
			this._bullet = this._bullets[this._currentBullet];
			if(_shooting){													//Shoot if user touches for a while/holds
	    		InvokeRepeating("Shoot", 0.1f, this._bullet._fireRate);
	    		}
		}
	
	}
	
	//Set weapon to the firs Bullet in the _bullets list (Happens when player dies)
	public void ResetWeapon() {
		_currentBullet=0;
		this._bullet = this._bullets[0];
	}
	
	
	public void LateUpdate() {
		/*if(hit.point != transform.position){
			Quaternion rotation = Quaternion.LookRotation(hit.point - transform.position);		//Calculate rotation
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _rotateSpeed);	//Rotate Player towards the touching point
		}
	    _moveLook.transform.LookAt(_moveTo.transform.position, transform.up);						//Player movement direction 
		 float d = Vector3.Distance(transform.position, _moveTo.transform.position);			//Distance between Player and where it is moving to
	   	GetComponent<Rigidbody>().velocity = _moveLook.transform.forward*_speed*d;									//Move the Player based on _moveLook rotation
	    if(d < .5f){																					//Disable _moveTo model if Player is close (GUI)
	    	if(_moveTo.activeInHierarchy)_moveTo.SetActive(false);														
	    }else if(_inControl){																		//Enable _moveTo model if not
	    	if(!_moveTo.activeInHierarchy) _moveTo.SetActive(true);
	   		_moveTo.transform.Rotate(new Vector3(0.0f,100*Time.deltaTime,0.0f));								//Rotate _moveTo model (visual)
	   	}
	*/

		if( CnInputManager.GetButtonDown("Jump") &&!_shooting){
		//if(Input.GetMouseButtonDown(0) &&!_shooting){												//Shoot if user touches for a while/holds
			InvokeRepeating("Shoot", 0.1f, this._bullet._fireRate);
		}else if( CnInputManager.GetButtonUp("Jump")){														//Move if user clicks/single touches game area
	//	}else if(Input.GetMouseButtonUp(0)){														//Move if user clicks/single touches game area
			StopShoot();																			//Stops shooting
			if((!_shooting && ((hit.collider.GetComponent<Pickup>() == null)||!_clickToPickup)) && Time.timeScale == 1){				
				_moveTo.transform.position = hit.point;												//Move the _moveTo to the touch position if no pickup is registered
			}else if(_clickToPickup && (hit.collider.GetComponent<Pickup>() != null)){
	//    		Debug.Log("Pickup:"+hit.collider.GetComponent(Pickup)._type);
				hit.collider.GetComponent<Pickup>().Pick();											//Pickup item		
			}	
			_shooting = false;																		//Player is not shooting
		}



	}
	
	
	public void Update() {
	   	#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE || UNITY_WEBPLAYER
	       pos = Input.mousePosition ;			//Get position based on mouse
	    #endif
	    #if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID || UNITY_WP8)
	    	if (Input.touchCount > 0)
	    	pos = Input.GetTouch(0).position ;		//Get position based on touch  
	    #endif
	    if(Physics.Raycast(Camera.main.ScreenPointToRay(pos), out hit)){ 
	   		 hit.point = new Vector3(hit.point.x, 0.0f , hit.point.z);
	    }
	    Debug.DrawLine(transform.position, hit.point);												//Draw a line to visualize where the user is touching on the game area (editor visual)	
	}
}
