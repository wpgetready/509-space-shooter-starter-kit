using UnityEngine;
using System;
using System.Collections;


public class MainMenu:MonoBehaviour
{
	public AudioClip _music;
	public string firstLevelToEnable;
	
	public IEnumerator Start() {
		yield return new WaitForSeconds(.1f);
		if(_music != null) SoundController.instance.PlayMusic(_music, 1.0f, 1.0f, true);
		//SaveStats.instance.createOrResetLevel ();
		SaveStats.instance.enableLevel (firstLevelToEnable);

		}
}
