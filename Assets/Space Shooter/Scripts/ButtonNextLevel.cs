using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Linq;


public class ButtonNextLevel:MonoBehaviour{
	//Button script used by all buttons in the game
	public AudioClip _buttonSound;			//Place sound clip for when button is pressed down
	public AudioClip _buttonSoundUp;		//Place sound clip for when button is released (this can be based on type of button)
	public bool _pauseButton;			//Enable for pause button
	public Mesh _playMesh;					//Mesh for play button after pause has been pressed
	public GameObject _pauseMenu;			//GameObject containing the buttons to be visible during pause
	public bool _quitButton;			//Enable for quit button (only level mode)
	public GameObject _quitMenu;			
	public bool _musicButton;			//Enable for music button
	public bool _soundButton;			//Enable for sound button
	public bool _nextTrackButton;		//Enable for track shuffle button
	public GameObject _offIcon;			//Place icon for on/off buttons
	public Camera _guiCamera;				//The gui camera(Ortographic) provides more accurate raycasting for small buttons
	
	public float _scaleDown = 0.9f;
	//public string _sceneToLoad;			//Enter the name of scene to load when button is pressed
	Vector3 _saveScale;		//Saves button scale
	Mesh _savePauseMesh;	//Saves pause button mesh
	
	
	public void Start() {

		_saveScale = transform.localScale;				//Save the scale for reset
		if(SaveStats.instance._saveMusicVol==0){
		SaveStats.instance._saveMusicVol = SoundController.instance._musicVol;
		SaveStats.instance._saveSoundVol = SoundController.instance._soundVol;
		}
		//Button types
		if(_musicButton && SoundController.instance._musicVol>0){
			_offIcon.GetComponent<Renderer>().enabled = false;
		} else if(_soundButton && SoundController.instance._soundVol>0){
			_offIcon.GetComponent<Renderer>().enabled = false;
		} else if(_pauseButton){
			_pauseMenu.SetActive(false);
			_savePauseMesh = transform.GetComponent<MeshFilter>().sharedMesh;
		} else if(_quitButton){
			_quitMenu.SetActive(false);
		}
		//Find best camera
		if(_guiCamera == null){
			GameObject camGO = GameObject.Find("_Gui Camera");
			if(camGO != null){
				_guiCamera = camGO.GetComponent<Camera>();
			}else{
				_guiCamera = Camera.main;
			}	
		}	
	}
	
	public void Update() {
		foreach(Touch touch in Input.touches) {
			RaycastHit hit = new RaycastHit() ;
			bool t = false;
			Ray ray = _guiCamera.ScreenPointToRay (touch.position);
			if (touch.phase == TouchPhase.Began && Physics.Raycast(ray,out hit,500000.0f)) {	
				if (hit.collider.gameObject == this.gameObject) {						//User touches a button
					OnButtonDown();
					t= true;
				}
			}
			if(t && touch.phase == TouchPhase.Ended){									//When user stops touching screen but not pressing button (button just resets, nothing happens)
				StartCoroutine(ButtonUp ());
			}
		}
	}
	#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE || UNITY_WEBPLAYER
	public void OnMouseUp() {
		transform.localScale = _saveScale;
	}
	#endif
	public IEnumerator ButtonUp() {
		string _sceneToLoad = SaveStats.instance._nextLeveL;
		if(_sceneToLoad != null){
			SceneManager.LoadScene(_sceneToLoad);
			Time.timeScale = 1.0f;
		}else if(_pauseButton){
			if(Time.timeScale > 0){
				Time.timeScale = 0.0f;
				_pauseMenu.SetActive(true);
				transform.GetComponent<MeshFilter>().sharedMesh = _playMesh;
				if(this._buttonSoundUp != null)
				SoundController.instance.Play(_buttonSoundUp, 2.0f, 1.0f);
			}else{
				_pauseMenu.SetActive(false);
				_quitMenu.SetActive(false);
				transform.GetComponent<MeshFilter>().sharedMesh = _savePauseMesh;
				Time.timeScale = .99f;
				yield return new WaitForSeconds(.1f);
				Time.timeScale = 1.0f;
			}
		}else if(_musicButton){
			if(SoundController.instance._musicVol > 0){
				SoundController.instance._musicVol = 0.0f;
				_offIcon.GetComponent<Renderer>().enabled = true;
			}else{
				SoundController.instance._musicVol = SaveStats.instance._saveMusicVol;
				_offIcon.GetComponent<Renderer>().enabled = false;
			}
			SoundController.instance.UpdateMusicVolume();
		}else if(_soundButton){
			if(SoundController.instance._soundVol > 0){
				SoundController.instance._soundVol = 0.0f;
				_offIcon.GetComponent<Renderer>().enabled = true;
			}else{
				SoundController.instance._soundVol = SaveStats.instance._saveSoundVol;
				_offIcon.GetComponent<Renderer>().enabled = false;
			}
		}else if(_nextTrackButton){
			GameController.instance.NextTrack();
			
		}else if(_quitButton){
			_quitMenu.SetActive(!_quitMenu.activeInHierarchy);
			
		}
	}
	
	public void OnMouseUpAsButton() {
		StartCoroutine(ButtonUp ());
	}
	
	public void OnButtonDown() {
		if(this._buttonSound != null)
		SoundController.instance.Play(_buttonSound, 2.0f, UnityEngine.Random.Range(1.5f,1.25f));	//Play a sound when pressing button
		transform.localScale = _saveScale * _scaleDown;							//Scale the button down to indicate button is being pressed
	}
	
	public void OnMouseDown() {
		OnButtonDown();
	}
}
