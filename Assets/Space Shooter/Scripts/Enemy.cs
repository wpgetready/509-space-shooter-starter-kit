using UnityEngine;
using System;



public class Enemy:MonoBehaviour{
	public float _hitpoints = 3.0f;			//How many hits can the enemy take before exploding
	public float _maxSpeed = 2.0f;			//Enemy max velocity
	public GameObject _shield;				//Shield pops up when enemy collides with something (bullets still do damage)
	public ParticleSystem _explosion;		//Explosion when enemy dies
	public GameObject _model;				//Model of enemy
	public Bullet _bullet;					//Bullet that enemy shoots
	public float _shootEverySecond = 2.0f;	//How often enemy shoots
	public float _shootChance = .5f;		//Chance that enemy fires off a bullet when shooting
	public int _points = 1000;				//Points player gets when enemy is defeated
	public float _rotateBackForce = 300.0f;	//How much force to add
	public bool _dropPickupOnHit = true;//Drops loot when hit by a bullet
	public float _dropChance = 1.0f;			//0=never 1=always
	public bool IAmABoss = false;				//Defines if this enemy is a boss

	float _shieldCounter;	//Counter to deactivate shield
	Vector3 _force;			//Force added to enemy to move it
	bool _dead;			
	
	public ParticleSystem _particleSystem;	//Particle system emitted from enemy
									
	public void Start() {
		GameController.instance._enemyCounter++;
		InvokeRepeating("RandomDirection", 0.0f , 2.0f);	//Change force direction every 2 seconds
		if(_shield != null){								//Deactivate shield
			_shield.SetActive(false);
		}
		InvokeRepeating("Shoot", _shootEverySecond*2 , _shootEverySecond);	//Begins shooting routine
	}
	
	
	public void Shoot() {
		if(UnityEngine.Random.value<.5f){																//Only shoots if value is smaller than _shootChance
			Quaternion rot = Quaternion.identity;						
			rot.SetLookRotation(Player.instance.transform.position - transform.position); 	//Rotate towards player
			Instantiate(_bullet, transform.position, rot);					//Create a bullet on the stage
		}
	}
	
	
	public void DestroyEnemy() {
		if(!_dead){													//Make sure that this function is only run once
			GameController.instance._enemyCounter--;				//Decrease enemycounter in GameController
			_explosion.transform.position = transform.position;		//Positiones the explosion on enemy position
			_explosion.Play();										//Play explosion particle system
			_dead = true;											//Enemy no longer alive but still active
			_model.SetActive(false);									
			this.GetComponent<Collider>().enabled = false;
			_particleSystem.Stop();
			_shield.SetActive(false);
			CancelInvoke();											//Stops shooting
			GetComponent<Rigidbody>().velocity*=.1f;									//Slows down speed
			GameController.instance.AddPoints(_points);				//Add points to score
				GameController.instance.InvokeEnemies ();				//Starts creating new enemies
				Destroy(gameObject, 1.0f);									//Removes enemy completely
			if (IAmABoss) {
				GameController.instance._bossCount += 1; //Increase boss count.
			}
		}
	}
	
	
	
	public void FixedUpdate() {
		
		GetComponent<Rigidbody>().AddForce(_force);								//Add movement to enemy	
		//Deactivated because enemy rigidbody is frozen on these axises
	//	if(transform.rotation.z < 0 ){
	//		transform.rigidbody.AddTorque(0,0,_rotateBackForce*Time.deltaTime *-transform.rotation.eulerAngles.z);
	//	}else if(transform.rotation.eulerAngles.z > 0){
	//		transform.rigidbody.AddTorque(0,0,_rotateBackForce*Time.deltaTime *-transform.rotation.eulerAngles.z);
	//	}
	//	if(transform.rotation.x < 0 ){
	//		transform.rigidbody.AddTorque(_rotateBackForce*Time.deltaTime *-transform.rotation.x,0,0);
	//	}else if(transform.rotation.x > 0){
	//		transform.rigidbody.AddTorque(_rotateBackForce*Time.deltaTime *-transform.rotation.x,0,0);
	//	}
		//Gradually rotates enemy to zero
		if(transform.rotation.y < 0 ){
			transform.GetComponent<Rigidbody>().AddTorque(0.0f,_rotateBackForce*Time.deltaTime *-transform.rotation.y,0.0f);
		}else if(transform.rotation.y > 0){
			transform.GetComponent<Rigidbody>().AddTorque(0.0f,_rotateBackForce*Time.deltaTime *-transform.rotation.y,0.0f);
		}
		
		//Makes sure the enemy velocity dont go faster than max speed
		if(GetComponent<Rigidbody>().velocity.sqrMagnitude > _maxSpeed){ 
	        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * _maxSpeed;
	    } 
	}
	
	
	public void OnCollisionEnter(Collision col) {
		if(_hitpoints<1){
			DestroyEnemy();			//Destroy enemy if it has no hitpoints left
		}else{
			ActivateShield();		//Activate shield (Shield is just visual and has no real effect)
			if(_dropPickupOnHit && col.gameObject.tag == "Bullet" && _dropChance > UnityEngine.Random.value)	//Drops item when hit by bullet
				GameController.instance.Drop(transform.position);
		}
	}
	
	
	public void DeActivateShield() {
		if(_shield != null){
			_shield.SetActive(false);	//Deactivates shield
		}
	}
	
	
	public void ActivateShield() {	//Activate shield (Shield is just visual and has no real effect)
		if(_shield != null){
			_shieldCounter = 0.0f;		//Resets the counter that deactivates shield
			_shield.SetActive(true);	
		}
	}
	
	
	public void LateUpdate() {
		if(_shieldCounter > 1){					//Check if it is time to deactivate the shield
			DeActivateShield();
		}else{
			_shieldCounter += Time.deltaTime;	//Increase shield counter
		}
		if(transform.position.y < -0.01f){												//Checks to see if enemy is on the game plane
			transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, 0.0f, Time.deltaTime*GameController.instance._asteroidAlignToPlaneSpeed), transform.position.z); //Gradually positiones the asteroid closer to the game plane
		}else{
			GameController.instance.CheckBounds(this.gameObject, this._particleSystem); 	// Check if the enemy is in the game area, moves it to the closest edge if it is outside
			transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);														// Keeps the enemy on the game plane
		}
	}
	
	
	public void RandomDirection() {				//Change the direction of the enemy
		_force = new Vector3((float)UnityEngine.Random.Range(-1000, 1000), 0.0f ,(float)UnityEngine.Random.Range(-1000, 1000)); 
	}
}
