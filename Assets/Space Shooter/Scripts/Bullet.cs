using UnityEngine;
using System;
using System.Collections;


public class Bullet:MonoBehaviour{
	public ParticleSystem _rocket;			//Bullet is built up entierly by particles
	public ParticleSystem _explosion;		//Explosion particle system on collision
	public float _rotationOffset;			//Shoot bullet sideways
	public float _RandomRotationOffset;	//Randomly shoot bullet sideways
	public float _spreadDelay = .25f;		//How long until the bullets loose their trajectory (A more visually pleasing way to limit the bullets range alowing them to live longer but less effective after delay)
	public float _spreadAmount = .5f;		//How much the bullets spread after spread delay
	public float _bulletLifetime = 3.0f;		//How long the bullet is alowed to live without hitting anything (seconds)
	public float _bulletPower = 1.0f;			//Hitpoints removed from hit object
	public float _fireRate = 0.1f;			//Bullets rate of fire, low = fast
	public AudioClip _audioBirth;			//Audio clip to play when object is created
	public Vector2 _pitchRandom = new Vector2(.5f,1.5f);
	public AudioClip _audioDeath;			//Audio clip to play when object is destroyed
	public bool _wrapGameBorders;		//Bullets that leaves game area are moved to the oposite side
	public float _bulletSpeed = 3.0f;			//Speed of bullets
	bool _spread;
	Quaternion rotTarget;	//Rotation used for direction
	
	
	public IEnumerator Start() {
		if(_audioBirth != null)		SoundController.instance.Play(_audioBirth, UnityEngine.Random.Range(.5f,1.0f), UnityEngine.Random.Range(_pitchRandom.x,_pitchRandom.y));	//Audio on birth
		//_rocket.startLifetime*=Random.Range(.5,1);	//Randomize the _rocket lifetime to make bullets look less clony
		
		Vector3 newEuler = GetComponent<Rigidbody>().rotation.eulerAngles;
		newEuler.y += _rotationOffset+UnityEngine.Random.Range(-_RandomRotationOffset, _RandomRotationOffset);
		Quaternion newQ = GetComponent<Rigidbody>().rotation;
		newQ.eulerAngles = newEuler;
		GetComponent<Rigidbody>().rotation = newQ;
		
		rotTarget = GetComponent<Rigidbody>().rotation;				//set rotation for direction
		
		StartCoroutine(Spread());									//Randomly rotates the gameobject to change direction (delayed)
		StartCoroutine(DestroyBullet ());							//Removes the bullet from the stage (delayed)
		
		yield return new WaitForSeconds(_bulletLifetime);       //disable the particle emission before destroying (visual)
		ParticleSystem.EmissionModule emission = _rocket.emission;
		emission.enabled = false;	
		GetComponent<Collider>().enabled = false;
	}
	
	//Removes the bullet from the stage
	public IEnumerator DestroyBullet() {
		yield return new WaitForSeconds(_bulletLifetime+.75f);	//Wait for _bulletLifetime before destroying the bullet (delayed so that the rocket particle system has time to stop emission)
			Destroy(this.gameObject);
	}
	
	//Randomly rotates the gameobject to change direction (delayed)
	public IEnumerator Spread() {
		yield return new WaitForSeconds(_spreadDelay);	//Wait for _spreadDelay before making bullet steer off course
		randomRot ();							
		_spread = true;							//Indicate that the bullet now should move towards the random rotation
	}
	
	public void OnCollisionEnter(Collision col) {
		GameObject e = (GameObject)Instantiate(_explosion.gameObject, transform.position, GetComponent<Rigidbody>().rotation);
		ParticleSystem.MainModule main = _rocket.main;
		Destroy(e, main.duration);      //Destroy the bullet (Delayed)

		ParticleSystem.EmissionModule emission = _rocket.emission;
		emission.enabled = false;																									//Disable the rocket particle system
		transform.GetComponent<Collider>().enabled = false;																				//Disable collider to prevent more collsions since rocket needs to stop before destroying
		if(_audioDeath != null)		SoundController.instance.Play(_audioDeath, UnityEngine.Random.Range(.5f,1.0f), UnityEngine.Random.Range(.5f,1.5f));		//Audio on death	
		//GameController.instance.Explosion(transform.position , 1, 1000);												//Create and force explosion that effects all nearby objects(exept other bullets)
		//col.rigidbody.AddTorque(Vector3(Random.Range(100,-100),Random.Range(100,-100),Random.Range(100,-100)));		//Add some rotation to the hit object
		if(col.transform.tag == "Asteroid")																				//Hit an asteroid
			col.transform.GetComponent<Asteroid>()._hitpoints-=_bulletPower;												//Reduce hitpoints of hit object based on bullet power
		else if(col.transform.tag == "Enemy")																			//Hit an Enemy
			col.transform.GetComponent<Enemy>()._hitpoints-=_bulletPower;													//Reduce hitpoints of hit object based on bullet power
	}
	
	public void FixedUpdate() {
		GetComponent<Rigidbody>().velocity = transform.forward*_bulletSpeed;															//Movement forward
		if(_spread)GetComponent<Rigidbody>().rotation = Quaternion.Lerp(GetComponent<Rigidbody>().rotation, rotTarget, Time.deltaTime*_spreadAmount);	//Rotates to a random rotation, rotation speed is based on _spreadAmount
		if(_wrapGameBorders) GameController.instance.CheckBounds(this.gameObject, _rocket);								//Checks if the bullet is outside the game area
	}
	
	//Randomly rotates the gameobject to change direction (smooth)
	public void randomRot() {
		 rotTarget = UnityEngine.Random.rotation;
	}
}
