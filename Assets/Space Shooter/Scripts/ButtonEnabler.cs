﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;

public class ButtonEnabler : MonoBehaviour {

	public GameObject numberEnabled; //object to activate if level is enabled (deactivate if disabled)
	public GameObject numberDisabled; //object to activate if level is disabled (dectivate if enable)

	private BoxCollider bc;
	private Button btn;
	public bool flag;

	public void Start () {
		bc = gameObject.GetComponent<BoxCollider> ();
		btn = gameObject.GetComponent<Button> ();
		flag = SaveStats.instance.isLevelEnabled (btn._sceneToLoad);
		Update2 (flag);
	}

	public void LateUpdate() {

	}

	public void Update2(bool value) {
		//yield return new WaitForSeconds(2f);
		numberEnabled.GetComponent<Renderer> ().enabled = flag;
		numberDisabled.GetComponent<Renderer> ().enabled = !flag;
			bc.enabled = value;
			Debug.Log (string.Format ("Enabling {0} en {1}", btn._sceneToLoad, value));
	}
}

//20171109: Algo completamente extraño está pasando y fallo en dilucidar que pasa: levelEnabled defie si es preciso habilitar o no los objetos.
//Esto está ok y funciona TAL CUAL LO NECESITO Y LO PREDIJE.
//El problema es que NO funciona, ignorando el valor , sin importar que sea true o false.
//Sin embargo es contradictorio: la variable levelEnabled responde correctamente en el momento de ejecución y claramente dice que está en FALSE, lo cual es a su vez INCORRECTO(!).
//La pregunta es: como es esto posible? Como es posible que pase? Que es lo que está sucediendo? que sentido tiene?
//Creo que la explicación más simple es como sigue:
//los niveles heredan de un prefab.
//Sin embargo, realicé cambios al mismo, pero nunca hice un apply.
//Porque si lo hago, tengo conflictos raros, como por ejemplo se me repiten todos los objetos en cada nivel y OBVIAMENTE no quiero esto.
//Lo que está sucediendo en realidad es la consecuencia de NO hacer un apply a un prefab que estoy usando como molde.
//Y lo que pasa es que estoy teniendo resultados completamente inesperado de un prefab que no está ajustado.
//Por muy loco que resulte.
//Ahora, cual es la solución a este problema?
/*
 * El problema está surgiendo de como manejo los objetos, como los actualizo y como funcionan los prefabs.
 * Primer workaround: cada boton es un prefab por separado, veamos que pasa.
 * Resultado: NO funciona.
 * Noto algo inesperado: tengo DOS objetos a uno lo activo y al otro lo desactivo. Por alguna razon uno de los objetos IGNORA completamente el comando, el otro desaparece.
 * Curiosamente es el efecto OPUESTO de lo que esperaba.
 * Prueba 2: hago invisible a los dos objetos. La rutina hace aparecer el objeto que le digo que NO debe aparecer(?)
 * Repito el proceso: todo sale de la manera no esperada.
 * Prueba 3: uso corutinas. El MISMO resultado, no importa si paso true o false, SIEMPRE pone el objeto en gris, lo cual carece completamente de sentido.
 * Sigo pensando que es COMPLETAMENTE ABSURDO LO QUE ESTA PASANDO y no le he encontrado una explicación razonable.
 * Hace horas que estoy envuelto en esto, no hay una manera SIMPLE de seguir adelante?
 * SE QUE ESTOY HACIENDO BIEN LAS COSAS PORQUE ESTA PASANDO ESTO?
 * */
