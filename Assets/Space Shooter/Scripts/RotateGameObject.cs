using UnityEngine;
using System;


public class RotateGameObject:MonoBehaviour
{
	//Simple script to rotate a gameObject
	public float xRot;
	public float yRot;
	public float zRot;
	
	public void Update() {
		transform.Rotate(new Vector3(xRot,yRot,zRot)*Time.deltaTime);
	}
}