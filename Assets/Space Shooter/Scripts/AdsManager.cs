﻿using UnityEngine;
using System;
using System.Collections;

using GoogleMobileAds.Api;
using GoogleMobileAds;

/// <summary>
/// Ads manager.Generic class for handling ads, since I put InterstitialAd and just by coincidence it already existed...
/// </summary>
public class AdsManager: MonoBehaviour {


	#if UNITY_ANDROID
		//https://developers.google.com/admob/unity/test-ads#enable_test_devices
		//string adUnitId =  "ca-app-pub-3940256099942544/1033173712";  //Test code
		string adUnitId =  "ca-app-pub-3654628576200837/2716482354"; //VALID code!
		string appId =  "ca-app-pub-3654628576200837~8947026390"; //Valid code!
	#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-3940256099942544/4411468910";
		string appId = "ca-app-pub-3940256099942544~1458002511";
	#else
		string adUnitId = "unexpected_platform";
		string appId = "unexpected_platform";
	#endif

	public bool testingDevice =false;
	private InterstitialAd interstitial;
	private AdRequest request;

	private string testDevice ="A33D58161467384F7451243DEAF47AF4";
	public static AdsManager instance; //instance no destroy.

	public void Start() {
		if (instance != null){
		Destroy (gameObject);			//Destroy if there is a SaveStats loaded
		}else{
		instance = this;				
		MobileAds.Initialize(appId);
		DontDestroyOnLoad (gameObject); //Keep from deleting this gameObject when loading a new scene
		}
	}

	public void OnApplicationQuit() {				//Ensure that the instance is destroyed when the game is stopped in the editor.
	instance = null;
	}

	public void RequestInterstitial()
	{
	if (interstitial !=null) {
			interstitial.Destroy();
		}
		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd (adUnitId);

		interstitial.OnAdLoaded += HandleOnAdLoaded; 		// Called when an ad request has successfully loaded.
		interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad; 		// Called when an ad request failed to load.
		interstitial.OnAdOpening += HandleOnAdOpened; 		// Called when an ad is shown.
		interstitial.OnAdClosed += HandleOnAdClosed; 		// Called when the ad is closed.
		interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication; 		// Called when the ad click caused the user to leave the application.

		// Create an empty ad request.
		if (!testingDevice) {
		request = new AdRequest.Builder().Build ();
		} else {
		//Test Devices: 
		//https://developers.google.com/admob/unity/test-ads#enable_test_devices
		request  = new AdRequest.Builder ()
		.AddTestDevice (AdRequest.TestDeviceSimulator)
		.AddTestDevice (testDevice)
		.Build ();

			Debug.Log ("Testing Device:" + testDevice);
		}
		Debug.Log ("FZ using adUnitId:" + adUnitId);
		Debug.Log ("FZ using appId:" + appId);

		// Load the interstitial with the request.
		interstitial.LoadAd (request);
	}

		/// <summary>
		/// Displays interstitial ad if possilbe.
		/// </summary>
		public  void DisplayIAd()
	{		

			if (interstitial.IsLoaded()) {
			Debug.Log ("FZ displaying Ad...");
				Time.timeScale = 0f; //Trick: this 'freezes' game while is loading
				interstitial.Show();
				Debug.Log ("FZ displayed Ad...");
			} else {
					Debug.Log ("FZ Interstitial failed!!");
			}
		}

		public void HandleOnAdLoaded(object sender, EventArgs args)
		{
			Debug.Log("FZ HandleAdLoaded event received");
		}

		public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
		{
			Debug.Log("FZ HandleFailedToReceiveAd event received with message: "		+ args.Message);
			Time.timeScale = 1.0f;
		}

		public void HandleOnAdOpened(object sender, EventArgs args)
		{
			Debug.Log("FZ HandleAdOpened event received");
		}

		public void HandleOnAdClosed(object sender, EventArgs args)
		{
			Debug.Log("FZ HandleAdClosed event received");
			Time.timeScale = 1.0f;
			interstitial.Destroy ();
		}

		public void HandleOnAdLeavingApplication(object sender, EventArgs args)
		{
			Debug.Log("FZ HandleAdLeftApplication event received");
			Time.timeScale = 1.0f;
			interstitial.Destroy ();	
		}
	}
