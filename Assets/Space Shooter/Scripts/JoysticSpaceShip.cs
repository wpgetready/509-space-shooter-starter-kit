﻿using CnControls;
using UnityEngine;
using System;
using UnityEngine.UI;


[RequireComponent(typeof(Rigidbody))]
public class JoysticSpaceShip : MonoBehaviour
    {
    
        private Vector3[] directionalVectors = { Vector3.forward, Vector3.back, Vector3.right, Vector3.left };

        private Transform _mainCameraTransform;
        public float Speed=3;
		public float joyTolerance; //Used to control how much ignore before deciding movement is impulse and rotation.
							//Recomended values: between 0 - 0.5

        private Vector3 Direction;
        
        private void Awake()
        {
            _mainCameraTransform = Camera.main.transform;
        }

        public void Update()
        {
		float hor = CnInputManager.GetAxis ("Horizontal");
		float ver = CnInputManager.GetAxis ("Vertical");
			//Tolerance to avoid constantly rotating
		if (Math.Abs(hor) <=joyTolerance) {
				hor =0;
			}
            var movementVector = new Vector3(hor, 0f,ver );

				//Debug.Log (CnInputManager.GetAxis ("Horizontal") + " - " + CnInputManager.GetAxis ("Vertical"));
            if (movementVector.sqrMagnitude < 0.00001f) return;

            // Clamping
            Vector3 closestDirectionVector = directionalVectors[0];
            float closestDot = Vector3.Dot(movementVector, closestDirectionVector);
            for (int i = 1; i < directionalVectors.Length; i++)
            {
                float dot = Vector3.Dot(movementVector, directionalVectors[i]);
                if (dot < closestDot)
                {
                    closestDirectionVector = directionalVectors[i];
                    closestDot = dot;
                }
            }

            // closestDirectionVector is what we need
            var transformedDirection = _mainCameraTransform.InverseTransformDirection(closestDirectionVector);
            transformedDirection.y = 0f;
            transformedDirection.Normalize();
            Direction = transformedDirection;
            if (transformedDirection.z < 0)
            {
                GetComponent<Rigidbody>().AddForce(transform.forward * -transformedDirection.z * Time.deltaTime * Speed*40);
            }
            transform.Rotate(Vector3.up * Speed *1.5f*movementVector.x);
            GameController.instance.CheckBounds(this.gameObject, null);

        }
    
}
