using UnityEngine;
using System;


public class TheEnd:MonoBehaviour{
	public GameObject _gameOver;				//Gameobject visible when player has died
	public GameObject _levelComplete;			//Gameobject visible when level complete
	public TextMesh _scoreTextMesh;			//Displays score from previous level
	public Button _retryButton;				//Insert retry button to go back to previously loaded level
	string _previousLevel;
	
	public void Start() {
//		_gameOver.SetActive(false);
		_levelComplete.SetActive(true);
		_scoreTextMesh.text = SaveStats.instance._score.ToString("000000000");							//Change score text
		_previousLevel = SaveStats.instance._previousLevel;
		if(SaveStats.instance._score > PlayerPrefs.GetInt(_previousLevel+"highscore", 0)){				// Check if score is higher than high score of last played level
			PlayerPrefs.SetInt(_previousLevel+"highscore", SaveStats.instance._score);					// Change save data high score of last played level
		
		}	
		if(SaveStats.instance._levelComplete){															//If player completed level
			_levelComplete.SetActive(true);
		}else if(!SaveStats.instance._levelComplete){
			//_gameOver.SetActive(true);																	//If player died in last level
		}
		if(_retryButton != null){
			_retryButton._sceneToLoad = _previousLevel;													//Replay last level
		}
	}
}
